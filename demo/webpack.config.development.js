const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    entry: './src/app.js',
    output: {
	filename: './dist/bundle.js',
    },
    devtool: 'source-map',
    resolve: {
	extensions: ['.js']
    },
    module: {
	rules: [
	    {test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/}
	]
    },
    resolve: {
	alias: {
	    react: path.resolve('./node_modules/react')
	}
    },
    plugins: [
	new HtmlWebpackPlugin({
	    title: 'test',
	    template: './src/index.html'
	})
    ]
};
