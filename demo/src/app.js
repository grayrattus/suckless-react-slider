import React, {useRef} from 'react';
import ReactDOM from 'react-dom';
import {createRoot} from 'react-dom/client';

import {withSlider} from '../../library/lib/index';

const SliderComponentFullWidth = () => <div style={{display: 'flex', width: 'max-content'}}>
  <div style={{ overflow: 'hidden', pointerEvents: 'none', width: '100vw', height: '700px', backgroundSize: 'cover', backgroundImage: 'url(https://futurama-student-website-project.vercel.app/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhug.8988f0cc.jpeg&w=1920&q=75)' }} />
  <div style={{width: '100vw', height: '700px', background: 'purple' }}>2</div>
  <div style={{width: '100vw', height: '700px', background: 'purple' }}>3</div>
  <div style={{ overflow: 'hidden', pointerEvents: 'none', width: '100vw', height: '700px', backgroundSize: 'cover', backgroundImage: 'url(https://futurama-student-website-project.vercel.app/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhug.8988f0cc.jpeg&w=1920&q=75)' }} />
  <div style={{width: '100vw', height: '700px', background: 'purple' }}>4</div>
  <div style={{ overflow: 'hidden', pointerEvents: 'none', width: '100vw', height: '700px', backgroundSize: 'cover', backgroundImage: 'url(https://futurama-student-website-project.vercel.app/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhug.8988f0cc.jpeg&w=1920&q=75)' }} />
</div>

const SliderComponentHalfWidth = () => <div style={{display: 'flex', width: 'max-content'}}>
  <div style={{width: '50vw', height: '300px', background: 'purple' }}>hello world</div>
  <div style={{width: '50vw', height: '300px', background: 'purple' }}>hello world</div>
  <div style={{width: '50vw', height: '300px', background: 'purple' }}>hello world</div>
</div>

const SliderComponentFullWidthMargins = () => <div style={{display: 'flex', width: 'max-content'}}>
  <div style={{width: 'calc( 100vw - 30px - 30px )', margin: '0 30px 0 30px',  height: '300px', background: 'purple' }}>hello world</div>
  <div style={{width: 'calc( 100vw - 30px - 30px )', margin: '0 30px 0 30px',  height: '300px', background: 'purple' }}>hello world</div>
  <div style={{width: 'calc( 100vw - 30px - 30px )', margin: '0 30px 0 30px',  height: '300px', background: 'purple' }}>hello world</div>
</div>

const ManySlidesOnOneSlider = () => <div style={{display: 'flex', width: 'max-content'}}>
  <div style={{width: 'calc( 50vw - 30px - 30px )', margin: '0 30px 0 30px',  height: '300px', background: 'purple' }}>hello world</div>
  <div style={{width: 'calc( 50vw - 30px - 30px )', margin: '0 30px 0 30px',  height: '300px', background: 'purple' }}>hello world</div>
  <div style={{width: 'calc( 50vw - 30px - 30px )', margin: '0 30px 0 30px',  height: '300px', background: 'purple' }}>hello world</div>
</div>

const AdvanedSlider = () => <div style={{width: '100vw' }}>
  <div className='row' style={{width: '100vw'}}>
      <div className='col-xs-4' >
	<div style={{  width: '100%', background: 'purple' }}>hello world</div>
      </div>
      <div className='col-xs-4' >
	<div style={{  width: '100%', background: 'purple' }}>hello world</div>
      </div>
      <div className='col-xs-4' >
	<div style={{  width: '100%', background: 'purple' }}>hello world</div>
      </div>
  </div>
</div>

const AdvancedSliderWithSlider = withSlider(AdvanedSlider, {
  paginate: {
    stickToSlide: true
  }
});


const SliderComponentFullWidthWithSlider = withSlider(SliderComponentFullWidth, {
   paginate: {
      stickToSlide: true,
      stopOnEdges: true

   }
});

const SliderComponentHalfWidthWithSlider = withSlider(SliderComponentHalfWidth, {
  paginate: {
    numberOfSlides: 3,
    stickToSlide: true
  }
});

const SliderComponentFullWidthMarginsWithSlider = withSlider(SliderComponentFullWidthMargins, {
  paginate: {
    numberOfSlides: 3,
    stickToSlide: true
  }
});

const ManySlidesOnOneSliderWithSlider = withSlider(ManySlidesOnOneSlider, {
  paginate: {
    numberOfSlides: 3,
    stickToSlide: true
  }
});

const App = () => {
      const slideToIndexCallbackRef = useRef(Function.prototype);
      return <div>
    <h2>Simple with pagination clicker</h2>
	<SliderComponentFullWidthWithSlider slideToIndexCallbackRef={slideToIndexCallbackRef} numberOfSlides={6} renderPaginationCallback={(currentIndex, renderIndex) => <div onClick={() => slideToIndexCallbackRef.current(renderIndex)} style={{ cursor: 'pointer', background: currentIndex === renderIndex ? 'pink' : 'none', position: 'relative'}}>{renderIndex}</div>} />
        <h2>Simple slider</h2>
	<SliderComponentFullWidthWithSlider numberOfSlides={6} renderPaginationCallback={(currentIndex, renderIndex) => <div style={{background: currentIndex === renderIndex ? 'pink' : 'none', position: 'relative'}}>{renderIndex}</div>} />
        <h2>Slider with margins</h2>
	<SliderComponentFullWidthMarginsWithSlider  numberOfSlides={3} renderPaginationCallback={(currentIndex, renderIndex) => <div style={{background: currentIndex === renderIndex ? 'pink' : 'none', position: 'relative'}}>{renderIndex}</div>} />
        <h2>Many slides on one slider</h2>
	<ManySlidesOnOneSliderWithSlider numberOfSlides={3} renderPaginationCallback={(currentIndex, renderIndex) => <div style={{background: currentIndex === renderIndex ? 'pink' : 'none', position: 'relative'}}>{renderIndex}</div>} />
    	<h2>Use flexbox grid and align columns</h2>
    	  <div className='row' style={{width: '100%'}}>
    	  <div className='col-xs-12 col-md-8 col-md-offset-4'>
    	    <AdvancedSliderWithSlider numberOfSlides={3} renderPaginationCallback={(currentIndex, renderIndex) => <div style={{background: currentIndex === renderIndex ? 'pink' : 'none', position: 'relative'}}>{renderIndex}</div>}/>
    	  </div>
    	</div>
        <h2>Sliders variations</h2>
        <div style={{width: '50%'}}>
          <SliderComponentHalfWidthWithSlider numberOfSlides={3} renderPaginationCallback={(currentIndex, renderIndex) => <div style={{background: currentIndex === renderIndex ? 'pink' : 'none', position: 'relative'}}>{renderIndex}</div>} />
        </div>
        <div style={{marginLeft: '50%', width: '50%'}}>
          <SliderComponentHalfWidthWithSlider numberOfSlides={3} renderPaginationCallback={(currentIndex, renderIndex) => <div style={{background: currentIndex === renderIndex ? 'pink' : 'none', position: 'relative'}}>{renderIndex}</div>} />
        </div>
        </div>
}

const container = document.createElement('div');
document.body.appendChild(container);
const root = createRoot(container)

document.body.style.margin = '0px';

root.render(<App />);
