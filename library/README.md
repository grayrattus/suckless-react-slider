# suckless-react-slider

I made this slider as a PoC to prove that you don't need `react-spring` or `gsap` to make working touch slider.

Principles of this project are:

- slider HOC should always be in one file so whoever needs to add new functionality can copy source code and customize it
- slider should use Hammer.js and CSS variables for transitions
- slider should use render props for pagination so it's easy to customize it

Demo of slider can be found [here](http://suckless-react-slider.dziedziczak-artur.xyz/)

