import Hammer from '@egjs/hammerjs';
import React, { useRef, useEffect, useState } from 'react';

export const withSlider = (Component, config = {
  paginate: {
    stickToSlide: true
  }
}) => ({ renderPaginationCallback, slideToIndexCallbackRef, numberOfSlides, ...props}) => {
  const divRef = useRef<HTMLDivElement>(null);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const position = useRef({x: 0});
  const [currentIndex, setCurrentIndex] = useState(0);

  const calcPercentForXPositionInPx = (xPosition: number, widthOfSingleSlide: number) => {
    return ((xPosition) / (widthOfSingleSlide)) * 100;
  }

  useEffect(() => {
    const divReference = divRef.current;
    if (divReference) {
      if (slideToIndexCallbackRef) {
        slideToIndexCallbackRef.current = (index) => {
          setCurrentIndex(index);
          const wrappedComponent = divReference.children[0];
          const widthOfSingleSlide = wrappedComponent.getBoundingClientRect().width / numberOfSlides;
	  position.current = {x: (index * (divReference.children[0].getBoundingClientRect().width) / numberOfSlides * -1)};
          divReference.style.setProperty('--transition', `transform 0.5s ease-in-out`);
          divReference.style.setProperty('--x-position', `${calcPercentForXPositionInPx(index * (divReference.children[0].getBoundingClientRect().width / numberOfSlides / numberOfSlides * -1), widthOfSingleSlide)}%`);
        }
      }
      divReference.style.setProperty('--x-position', '0px');
      const hammertime = new Hammer.Manager(divReference);
      const PanEvent = new Hammer.Pan({ event: 'pan-new', direction: Hammer.DIRECTION_HORIZONTAL });
      hammertime.add(PanEvent);
      hammertime.on('pan-new', function(ev) {
        const wrappedComponent = divReference.children[0];
        let index = -Math.round((position.current.x + ev.deltaX) / (wrappedComponent.getBoundingClientRect().width / numberOfSlides));

        if (index >= numberOfSlides) {
          index = numberOfSlides - 1;
        } else if (index < 0) {
          index = 0;
        }
        setCurrentIndex(index);

        const widthOfSingleSlide = wrappedComponent.getBoundingClientRect().width / numberOfSlides;
	const shouldBlockSwipeToLeft = position.current.x + ev.deltaX > 0 && index === 0;

	if (shouldBlockSwipeToLeft) {
		divReference.style.setProperty('--transition', `transform 0.5s ease-in-out`);
	        divReference.style.setProperty('--x-position', `${0}%`);
	} else if (position.current.x + ev.deltaX <= -(wrappedComponent.getBoundingClientRect().width - (wrappedComponent.getBoundingClientRect().width / numberOfSlides))) {
		divReference.style.setProperty('--transition', `transform 0.5s ease-in-out`);
	        divReference.style.setProperty('--x-position', `${calcPercentForXPositionInPx(-(wrappedComponent.getBoundingClientRect().width - (widthOfSingleSlide)), widthOfSingleSlide)}%`);
	} else if (position.current.x + ev.deltaX >= 0) {
		divReference.style.setProperty('--transition', `transform 0.5s ease-in-out`);
	        divReference.style.setProperty('--x-position', `${0}%`);
	}

	if (ev.isFinal) {
	        if (config.paginate.stickToSlide) {
	      	  position.current = {x: (index * (divReference.children[0].getBoundingClientRect().width) / numberOfSlides * -1)};

	      	  divReference.style.setProperty('--transition', `transform 0.5s ease-in-out`);
	      	  divReference.style.setProperty('--x-position', `${calcPercentForXPositionInPx((index * (divReference.children[0].getBoundingClientRect().width / numberOfSlides * -1)) / numberOfSlides, widthOfSingleSlide)}%`);
	        } else {
	      	  position.current = {x: position.current.x + ev.deltaX};
	      	  divReference.style.setProperty('--transition', `transform 0.5s ease-in-out`);
	      	  divReference.style.setProperty('--x-position', `${calcPercentForXPositionInPx(position.current.x, widthOfSingleSlide)}%`);
	        }
	} else if (!(shouldBlockSwipeToLeft)) {
	        divReference.style.setProperty('--transition', `transform 0s linear`);
	        divReference.style.setProperty('--x-position', `${calcPercentForXPositionInPx((position.current.x + ev.deltaX) / numberOfSlides, widthOfSingleSlide)}%`);
	}
      });
    }
  }, []);

  return <div ref={wrapperRef} style={{overflow: 'hidden'}}>
    <div style={{ width: 'max-content' }}>
      <div style={{ transition: 'var(--transition)', transform: 'translateX(var(--x-position))'}} ref={divRef} >
      <Component {...props} />
      </div>
    </div>
    {Array(numberOfSlides).fill(1).map((_, i) => typeof renderPaginationCallback === 'function' ? renderPaginationCallback(currentIndex, i) : null) }
  </div> 

}
